DESCRIPTION += " with browsers support"
LICENSE = "MIT"

IMAGE_FEATURES += "splash package-management hwcodecs ssh-server-openssh"

inherit core-image distro_features_check

IMAGE_LINGUAS = "en-us es-es"
GLIBC_GENERATE_LOCALES = "en_US.UTF-8 es_ES.UTF-8"

# IMAGE_OVERHEAD_FACTOR = "1.5"
# IMAGE_ROOTFS_EXTRA_SPACE = "2097152"

EXTRA_IMAGE_FEATURES .= " debug-tweaks"
## EXTRA_IMAGE_FEATURES .= " debug-tweaks dbg-pkgs tools-debug tools-profile"
# By default, the Yocto build system strips symbols from the binaries it
# packages, which makes it difficult to use some of the tools.
# 
# You can prevent that by setting the INHIBIT_PACKAGE_STRIP variable to "1" in
# your local.conf when you build the image:
INHIBIT_PACKAGE_STRIP = "1"

IMAGE_INSTALL_append = " libtasn1 htop nano strace bridge-utils \
    ntp curl dhcp-client lzo \
    browserperfrunner \
    e2fsprogs-e2fsck e2fsprogs-mke2fs e2fsprogs-tune2fs e2fsprogs-badblocks e2fsprogs-resize2fs \
    parted pv \
    perf \
    smem \
    gdb \
    gdbserver \
    "


SDK_EXTRA_TOOLS += "nativesdk-cmake nativesdk-ninja \
    nativesdk-perl-module-findbin \
    nativesdk-perl-misc \
    nativesdk-wayland-dev \
    nativesdk-gperf \
    nativesdk-ruby \
    "
TOOLCHAIN_HOST_TASK_append = "${SDK_EXTRA_TOOLS}"
TOOLCHAIN_TARGET_TASK_append = " openjpeg-staticdev"
TOOLCHAIN_TARGET_TASK_remove = "target-sdk-provides-dummy"

PACKAGECONFIG_append_pn-php = " apache2"

# Allow dropbear/openssh to accept root logins if debug-tweaks or allow-root-login is enabled
ROOTFS_POSTPROCESS_COMMAND += "ssh_internal_sftp; "

#
# Set a valid internal-sftp
#
ssh_internal_sftp () {
        for config in sshd_config sshd_config_readonly; do
                if [ -e ${IMAGE_ROOTFS}${sysconfdir}/ssh/$config ]; then
                        sed -i 's/^[#[:space:]]*Subsystem sftp.*/Subsystem sftp internal-sftp/' ${IMAGE_ROOTFS}${sysconfdir}/ssh/$config
                fi
        done
}

