DESCRIPTION = "core-image-weston"

inherit image_browsers
inherit ${@bb.utils.contains('DISTRO_FEATURES','sota','image_browsers_ostree','', d)}

REQUIRED_DISTRO_FEATURES = "opengl wayland"
CORE_IMAGE_BASE_INSTALL += "weston weston-init weston-examples"

IMAGE_INSTALL_append = " waylandeglinfo"
IMAGE_INSTALL_append = " chromium-ozone-wayland"
