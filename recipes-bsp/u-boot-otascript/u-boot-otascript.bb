DESCRIPTION = "Boot script for launching OTA-enabled images on wandboard"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

DEPENDS = "u-boot-mkimage-native"

PR = "r1"

COMPATIBLE_MACHINE = "wandboard"

SRC_URI = "file://boot.scr \
	   file://uEnv.txt"

S = "${WORKDIR}"

inherit deploy

FILES_${PN} += "/boot/*"

do_install() {
    install -d ${D}/boot

    mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Ostree boot script" -d ${S}/boot.scr ${D}/boot/boot.scr
    install -m 0755 ${S}/uEnv.txt ${D}/boot/uEnv.txt
}

do_deploy() {
    install -d ${DEPLOYDIR}/wandboard-bootfiles

    mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "Ostree boot script" -d ${S}/boot.scr ${DEPLOYDIR}/wandboard-bootfiles/boot.scr
    install -m 0755 ${S}/uEnv.txt ${DEPLOYDIR}/wandboard-bootfiles/uEnv.txt
}

addtask deploy before do_package after do_install
do_deploy[dirs] += "${DEPLOYDIR}/wandboard-bootfiles"

PACKAGE_ARCH = "${MACHINE_ARCH}"
