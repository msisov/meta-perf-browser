FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILESEXTRAPATHS_prepend_class-devupstream := "${THISDIR}/files:"

SRC_URI += " file://cog-fdo \
    file://cog-set-opaque-region.patch \
"
SRC_URI_class-devupstream += " file://cog-fdo \
    file://cog-set-opaque-region.patch \
"

do_install_append () {
    install -d ${D}/${bindir}/
    install -m 755 ${WORKDIR}/cog-fdo ${D}/${bindir}/cog-fdo
}

SRCREV_class-devupstream = "${AUTOREV}"

PV_class-devupstream = "trunk"

RDEPENDS_${PN} += "bash"
RDEPENDS_${PN}_class-devupstream += "bash"
