FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

S_class-devupstream = "${WORKDIR}/git"
SRC_URI_class-devupstream = "git://github.com/WebKit/webkit.git;protocol=git;branch=master \
    file://0001-Use-kinetic-scrolling-code-on-async-rendering-path.patch \
"
PACKAGECONFIG[accessibility] = "-DENABLE_ACCESSIBILITY=ON,-DENABLE_ACCESSIBILITY=OFF,atk at-spi2-atk"

# FIXME: Trunk is borken and unstable since
# https://bugs.webkit.org/show_bug.cgi?id=203290. Fix the SRCREV in the
# latest revision with a build success reported temporary.
SRCREV_class-devupstream = "cc095d4f8ee860aaa16d363e7b2f23ea9c44ac52"
# SRCREV_class-devupstream = "${AUTOREV}"

PV_class-devupstream = "trunk"

RCONFLICTS_${PN}_class-devupstream = ""
