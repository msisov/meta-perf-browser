do_install_append() {
   ln -s ${libdir}/chromium/chromium-wrapper ${D}${bindir}/chrome
}

FILES_${PN} += "${bindir}/chrome"
