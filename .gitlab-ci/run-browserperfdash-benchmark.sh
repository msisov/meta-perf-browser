#!/bin/bash -x

RUN_DIR=$PWD
WEBKIT_DIR="webkit"

if [ ! -d "${WEBKIT_DIR}" ]
then
   wget -q -O WebKit-SVN-source.tar.bz2 https://s3-us-west-2.amazonaws.com/archives.webkit.org/WebKit-SVN-source.tar.bz2
   tar -xf WebKit-SVN-source.tar.bz2
   rm -rf webkit/WebKitBuild/Release/ WebKit-SVN-source.tar.bz2
fi

cd ${WEBKIT_DIR}

export WEBKIT_TEST_CHILD_PROCESSES=2
export WAYLAND_DISPLAY=wayland-0
export XDG_RUNTIME_DIR=/tmp/xdg-${UID}-runtime-dir/
export NUMBER_OF_PROCESSORS=2
export LD_LIBRARY_PATH=WebKitBuild/Release/lib/

PLAN="--allplans"
if [ ${2} != "allplans" ]
then
    PLAN="--plan ${2}"
fi

REVISION="trunk.$(svn info --show-item revision)"
if [ -n "${3}" ]
then
    REVISION="$(${1} --version) $(date +"%Y%m%d")"
fi

browserperfdash-benchmark  \
		${PLAN} \
		--timeout-factor 20  \
		--config-file /etc/browserperfdash-benchmark.cfg \
		--browser ${1} \
        --browser-version "${REVISION}" 2>&1 \
		| pv  -l -i 30  > ${RUN_DIR}/run-browserperfdash-benchmark_${1}_${2}.txt
