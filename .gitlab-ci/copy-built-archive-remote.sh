#!/bin/bash -x
REMOTE_WORKER_SSH_HOST=$1
REMOTE_WORKER_SSH_WORKDIR=$2
REMOTE_WORKER_SSH_OPT=$3

WEBKIT_DIR="webkit"

cd ${WEBKIT_DIR}
### Build product archive webkit
python ./Tools/BuildSlaveSupport/built-product-archive --platform=wpe --release archive
### Copy product archive webkit
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "mkdir -p ${REMOTE_WORKER_SSH_WORKDIR}/${WEBKIT_DIR}/WebKitBuild/"
scp ${REMOTE_WORKER_SSH_OPT}  WebKitBuild/release.zip ${REMOTE_WORKER_SSH_HOST}:${REMOTE_WORKER_SSH_WORKDIR}/${WEBKIT_DIR}/WebKitBuild/
### Extract product archive webkit
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "cd ${REMOTE_WORKER_SSH_WORKDIR}/${WEBKIT_DIR}; python ./Tools/BuildSlaveSupport/built-product-archive --platform=wpe --release extract"
