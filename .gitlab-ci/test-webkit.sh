#!/bin/bash -x

RUN_DIR=$PWD
WEBKIT_DIR="webkit"

if [ $# -gt 0 ]
then
    WEBKIT_DIR=${1}
fi

if [ ! -d "${WEBKIT_DIR}" ]
then
   wget -q -O WebKit-SVN-source.tar.bz2 https://s3-us-west-2.amazonaws.com/archives.webkit.org/WebKit-SVN-source.tar.bz2
   tar -xf WebKit-SVN-source.tar.bz2
   rm -rf webkit/WebKitBuild/Release/ WebKit-SVN-source.tar.bz2
fi

cd ${WEBKIT_DIR}

export WEBKIT_TEST_CHILD_PROCESSES=2
export WAYLAND_DISPLAY=wayland-0
export XDG_RUNTIME_DIR=/tmp/xdg-${UID}-runtime-dir/
export NUMBER_OF_PROCESSORS=2

python ./Tools/Scripts/run-webkit-tests --no-show-results \
        --no-new-test-results --no-sample-on-timeout \
		--results-directory layout-test-results \
        --debug-rwt-logging --release --wpe \
		--no-retry-failures --no-http-servers \
		--time-out-ms=30000 --exit-after-n-crashes-or-timeouts 1000 2>&1 \
		| pv  -l -i 30  > ${RUN_DIR}/test-webkit-results.txt

