#!/bin/bash

WEBKIT_DIR="webkit"

if [ $# -gt 0 ]
then
    WEBKIT_DIR=${1}
fi

if [ ! -d "${WEBKIT_DIR}" ]
then
   wget -q -O WebKit-SVN-source.tar.bz2 https://s3-us-west-2.amazonaws.com/archives.webkit.org/WebKit-SVN-source.tar.bz2
   tar -xf WebKit-SVN-source.tar.bz2
   rm -rf webkit/WebKitBuild/Release/ WebKit-SVN-source.tar.bz2
fi
