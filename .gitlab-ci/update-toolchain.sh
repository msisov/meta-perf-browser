#!/bin/bash -e

SDK_DIR="toolchain_env"

if [ -d "${SDK_DIR}" ]
then
  echo "SDK already exists"
  exit 0
fi

wget -q -O toolchain.sh \
   https://wk-contrib.igalia.com/yocto/meta-perf-browser/browsers-sota/stable/sdk/wandboard-mesa/browsers-sota-glibc-x86_64-core-image-wpe-base-armv7at2hf-neon-wandboard-mesa-toolchain-1.0.sh

chmod +x toolchain.sh
./toolchain.sh -d ${SDK_DIR} -y
rm toolchain.sh
