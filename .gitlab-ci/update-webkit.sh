#!/bin/bash

WEBKIT_DIR="webkit"

if [ $# -gt 0 ]
then
    WEBKIT_DIR=${1}
fi

if [ ! -d "${WEBKIT_DIR}" ]
then
   echo "error: not ${WEBKIT_DIR} found in the system"
   exit -1
fi

cd ${WEBKIT_DIR}
./Tools/Scripts/update-webkit
