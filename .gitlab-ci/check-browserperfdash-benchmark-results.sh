#!/bin/bash

RUN_DIR=$PWD

grep -n -B 28 -e "Timeout alarm was triggered" ${RUN_DIR}/run-browserperfdash-benchmark_${1}_${2}
t=$?

grep -n -C 1 -e "ERROR" ${RUN_DIR}/run-browserperfdash-benchmark_${1}_${2}
e=$?

if [[ $t && $e ]]
then
    echo "browserperfdash-benchmark (${1}) OK for ${RUN_DIR}/run-browserperfdash-benchmark_${1}_${2}.txt"
else
    echo "browserperfdash-benchmark (${1}) KO for ${RUN_DIR}/run-browserperfdash-benchmark_${1}_${2}.txt"
	exit 1
fi
