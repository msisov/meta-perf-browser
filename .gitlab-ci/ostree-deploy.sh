#!/bin/bash -x
REMOTE_URL_BASE="https://wk-contrib.igalia.com/yocto/meta-perf-browser/browsers-sota/"

REMOTE_WORKER_SSH_HOST=$1
REMOTE_WORKER_SSH_WORKDIR=$2
REMOTE_WORKER_SSH_OPT=$3
REMOTE=$4
REF=$5

ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "rm /var/log/ostree.log"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "systemctl restart ostree-provisioner"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree remote delete ostree_provisioner_stable || true"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree remote add --set=gpg-verify=false ostree_provisioner_stable ${REMOTE_URL_BASE}/stable/ostree/repo/"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree remote delete ostree_provisioner_nightly || true"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree remote add --set=gpg-verify=false ostree_provisioner_nightly ${REMOTE_URL_BASE}/nightly/ostree/repo/"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree pull ${REMOTE} ${REF}"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree admin deploy ${REMOTE}:${REF}"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/bin/ostree admin status"
ssh ${REMOTE_WORKER_SSH_OPT}  ${REMOTE_WORKER_SSH_HOST} "/usr/sbin/reboot"
sleep 60
exit 0
