FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://tools_add.patch"

PACKAGECONFIG[tools-all] = "-Dtools=all"
# PACKAGECONFIG[tools] = "-Dtools=calibrator,debug,info,terminal,touch-calibrator,-Dtools=false"
# [calibrator,debug,info,terminal,touch-calibrator]

PACKAGECONFIG_append = " clients tools-all"
